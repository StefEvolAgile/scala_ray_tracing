# Création image 3D sur le principe du Ray Tracing

## Introduction

Ce programme permet de créer une scène 3D constituée d'objets colorés, affichés et éclairés selon le principe du Ray Tracing.

## Fonctionnalités

- Création d'images

- Création de vidéo

- Rotation de la caméra

- Création d'objets (sphères et damiers)

- Création de sphères aléatoirement

- Modification de la couleur et de la taille des sphères

- Rebondissement des sphères avec une cinématique “gravitationelle” 

- Modification du nombre de sources lumineuses (1 ou 2)

- Réfléchissement de la lumière sur les objets en fonction de leur matière

- Mesure de la durée d'exécution totale du programme et de ses sous parties

- Affichage de la progression du programme au cours de son exécution 

## Utilisation du programme

#### Utilisation de Sbt

Sbt est un outil de construction open source pour les projets Scala, il permet la compilation du code Scala.

- Installation Linux : 

```shell
sudo apt install sbt
```

- Installation Windows : [lien](https://github.com/sbt/sbt/releases/download/v1.5.0/sbt-1.5.0.msi)

#### Utilisation de ffmpeg

ffmpeg est une collection de logiciels libres destinée au traitement de flux audio ou vidéo. 

- Installation Linux : 

```shell
sudo apt install ffmpeg
```

- Installation Windows : [lien](https://ffmpeg.org/download.html)

#### Compiler et exécuter

Afin d'exécuter le code il est nécessaire de se placer à la racine du projet puis d'ouvrir un terminal (powerShell sous Windows)  et de réaliser la commande ***sbt run***.

```shell
sbt run
```

Le programme demande le nombre d'images souhaité pour la scène de démonstration. Il est également possible de calculer une scène aléatoire en tapant **Enter** directement.

Dans la console vous pourrez observer l'avancement du programme à tout moment en visualisant le nombre d'image créer sur le nombre total d'image à réaliser. 

À la fin de l'exécution, le temps total de l'exécution du programme ainsi que celui de la réalisation des images et de la convertion vidéo seront visualisable.

#### Modification des paramètres

Il est possible de modifier plusieurs paramètres. Pour ce faire il faut changer manuellement des variables dans le code source du programme.  L'ensemble des modifications se font dans le fichier **Main.scala** dans l'**object Main**.

- Modification de la largeur de l'image (en pixel) : ligne 30

```scala
val largeur = 360
```

- Modification de la hauteur de l'image (en pixel) : ligne 31

```scala
val hauteur = 240
```

- Modification du nombre de lampes : Ligne 32

```scala
// true : 2 lampes 
// false : 1 lampe
val doubleLampe = true 
```

- Rotation de la caméra à chaque image (en degrés) : Ligne 33

```scala
val rotationDelta = 0.5
```

- Modification de la couleur d'une sphère :

Il faut modifier le ***Couleur.noir*** avec les couleurs disponible :

            - blanc, noir, rouge, vert, bleu, bleuCY

Ou alors en RVB sous Couleur(x,y,z) avec $(x,y,z) \in [0,1]^3$

```scala
val sphere56 = Sphere(Vec(1, -12, 2), 0.5, new Matiere(Couleur.noir, Couleur.blanc, Couleur.blanc, Couleur.blanc, 16, 0.5))  
```

**Note :** Attention lors de la saisie des paramètres, un nombre trop important d'images de haute qualité peut entrainer un temps calcul considérable. Les performances de votre ordinateur pourront accentuer le temps de calcul. 

#### Résultat final

Une fois la modification des paramètres réalisée et l'exécution terminée, vous trouverez les images générées dans le dossier **images_tmp** situé à la racine du projet. Ces images sont sous le format **XXXX.ppm** représentant l'ordre d'édition de l'image. 

La vidéo quant à elle se situe directement à la racine du projet sous un format mp4 et porte le nom **ray_tracing.mp4**. Il est possible de la visualiser avec des lecteurs vidéo comme VLC media player. 

**Note:** Vous trouverez toutes les informations nécessaires à la compréhension des algorithmes dans le fichier [NYC_CHAP_6_IMPRIMABLE_4.pdf](https://physique.cmaisonneuve.qc.ca/svezina/nyc/note_nyc/NYC_CHAP_6_IMPRIMABLE_4.pdf)
