package ray_tracing
import scala.util.Random

// Classe décrivant une couleur sous forme de vecteur de composantes RGB
// Les valeurs sont réelles et comprises entre 0 et 1
case class Couleur(r: Double, v: Double, b: Double) {
    def *(t: Double): Couleur = Couleur(r*t, v*t, b*t)
    def +(c: Couleur): Couleur = Couleur(Math.min(r+c.r, 1), Math.min(v+c.v, 1), Math.min(b+c.b, 1)) //saturation si on dépasse 1
    def mul(c: Couleur): Couleur = Couleur(r*c.r, v*c.v, b*c.b)
}

// Constantes utiles pour la classe couleurs
object Couleur {
    def randomCouleur() : Couleur = {
        val cr1 = Random.between(0,100)/100.toFloat
        val cr2 = Random.between(0,100)/100.toFloat
        val cr3 = Random.between(0,100)/100.toFloat
        Couleur(cr1, cr2, cr3) 
    } 

    val blanc = Couleur(1, 1, 1)
    val noir = Couleur(0, 0, 0)
    val rouge = Couleur(1, 0, 0)
    val vert = Couleur(0, 1, 0)
    val bleu = Couleur(0, 0, 1)
    val bleuCY = Couleur(0.5, 0.6, 1)
}