import ray_tracing._
import scala.sys.process._
import java.io._
import scala.io.Source
import scala.util.Random
object Main extends App{
    println("Bienvenue dans le monde du Ray Tracing !")
    println("Choisissez le nombre d'images de la scène de démonstration (ou Enter pour une scène aléatoire)")
    print("Nombre d'images : ")
    val entree = try {scala.io.StdIn.readInt()} catch {case e: Exception => 0}
    val modeRandom = (entree == 0) // Sélection du mode random ou décor complet 
    val nbImage = if (modeRandom) 1 else entree
    val nomTmpDir = "images_tmp"

    class TmpDir(nom : String) {
        val f = new File(nom)
        def detruit() {
            def delete(file: File) {
                if (file.isDirectory) 
                    Option(file.listFiles).map(_.toList).getOrElse(Nil).foreach(delete(_))
                file.delete
            }
            delete(f)
        }
        def construit() {
            f.mkdir()
        }
    }

    val largeur = 1920
    val hauteur = 1080
    val doubleLampe = true
    val rotationDelta = 0.5

    val damier = Damier(Vec(0, 0, 0), Vec(1, -1, 0).unitaire, Vec(1, 1, 0).unitaire, 
        new Matiere(Couleur(0.6, 0.6, 0.7), Couleur.blanc, Couleur.blanc, Couleur.blanc, 100, 0), 
        new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 100, 0))
    val lampe1 = Lampe(Vec(1, 10, 20), Couleur.blanc*0.8)
    val lampe2 = Lampe(Vec(0, -20, 10), Couleur.blanc*0.8)
    val listOfLampes = if (doubleLampe) List(lampe1, lampe2) else List(lampe1)

    //  Ajout de la lumière ambiante
    val lumiereAmbiante = Couleur.blanc*0

    val nbSpheres = Random.between(5, 40)
    //  Création de sphères randoms
    val spheresRandom = List.tabulate(nbSpheres)((i: Int)=> 
        Sphere(Vec(Random.between(3, 40), ((i%2)*2-1)*(i+1)/2*3, Random.between(1, 3)), 
            Random.between(0.5, 1),
            new Matiere(Couleur.randomCouleur(), Couleur.blanc, Couleur.blanc, Couleur.blanc, 64, 1)))
    
    val sphere54 = Sphere(Vec(5, 0, 1), 1.0, new Matiere(Couleur.rouge, Couleur.blanc, Couleur.blanc, Couleur.blanc, 64, 1))
    val sphere55 = Sphere(Vec(20, -20, 8), 5.0, new Matiere(Couleur.bleu, Couleur.blanc, Couleur.blanc, Couleur.blanc, 5, 0.5))
    val sphere56 = Sphere(Vec(1, -12, 2), 0.5, new Matiere(Couleur.noir, Couleur.blanc, Couleur.blanc, Couleur.blanc, 16, 0.5))
    
    // Création de la scene en fonction du temps
    def usineScene(t: Int): Scene = {
        def sphereRebond(t: Int, centre0: Vec, r: Double, m: Matiere): Sphere =  {
            val k = 0.01
            val z0 = Math.max(r+1, centre0.z) // Si jamais boule en dessous de la surface, on la remonte, r + 1 pour éviter période nulle 
            val periode = Math.sqrt((z0 - r)/k)
            val n = (t / periode).toInt
            val t2 = if (n%2 == 0) t - n*periode else (n+1)*periode - t 
            val z = Math.max(r, z0 - k * (t2 * t2) )
            Sphere(Vec(centre0.x, centre0.y, z), r, m)
        }
        val sphere1 = sphereRebond(t, Vec(-5, -10, 6), 1.0, new Matiere(Couleur.rouge, Couleur.blanc, Couleur.blanc, Couleur.blanc, 64, 1))
        val sphere2 = sphereRebond(t, Vec(-15, -5, 6), 2.0, new Matiere(Couleur.bleu, Couleur.blanc, Couleur.blanc, Couleur.blanc, 5, 0.5))
        val sphere3 = sphereRebond(t, Vec(-20, -20, 5), 3.0, new Matiere(Couleur.vert, Couleur.blanc, Couleur.blanc, Couleur.blanc, 16, 0))
        val sphere4 = sphereRebond(t,Vec(-12, -9, 10), 0.5, new Matiere(Couleur.noir, Couleur.blanc, Couleur.blanc, Couleur.blanc, 16, 0.5))
        
        // Scène contenant des sphères randoms ou la scène finale
        val listOfElement:List[Element] = if(modeRandom) List(damier).concat(spheresRandom) else
            List(damier,sphere1,sphere2,sphere3,sphere4, sphere54,sphere55,sphere56).concat(SetupCyTech.listeCYTECH)

        Scene(listOfElement, listOfLampes, lumiereAmbiante)
    }
    // Création du traceur 
    val traceur = new RayCasting()
    // Création de la caméra
    val camera = Camera(Vec(0, 0, 2), Vec(1, 0, 0).unitaire, Vec(0, 0, 1), 60, 1)

    // Prise du temps t0
    println("Début des calculs")
    val t0 = System.nanoTime()

    val tmpDir = new TmpDir(nomTmpDir)
    // Suppression du dossier si déja existant
    tmpDir.detruit()
    // Création du dossier temporaire
    tmpDir.construit()
    
    // Création des images
    def usineImage(t: Int) = {
        println("  Image " + (t + 1) + "/" + nbImage)
        traceur.calculeImage(largeur, hauteur, usineScene(t), camera.rot(t*rotationDelta))
    }
    
    // Print des images dans un fichier  
    Range(0, nbImage).map(t => usineImage(t).creerFichier(nomTmpDir + "/" + "%04d".format(t)))

    val t1 = System.nanoTime()
    

    //Conversion des images ppm en vidéo mp4
    if (!modeRandom) {
        Seq("ffmpeg", "-i", nomTmpDir + "/%04d.ppm", "-pix_fmt", "yuv420p", "-y", "ray_tracing.mp4").!
        val t2 = System.nanoTime()
        println("Fin de conversion, temps écoulé pour la conversion en mp4 : " + (t2 - t1)/1000000 + " ms")
        println("Fin de calcul, temps total écoulé: " + (t2 - t0)/1000000 + " ms")
    }
}
