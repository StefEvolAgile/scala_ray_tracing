package ray_tracing

// Classe de calcul vectoriel
case class Vec(x: Double, y: Double, z: Double) {
    def *(t: Double): Vec = Vec(x*t, y*t, z*t)
    def +(v: Vec): Vec = Vec(x+v.x, y+v.y, z+v.z)  
    def -(v: Vec): Vec = Vec(x-v.x, y-v.y, z-v.z)  
    def unary_- : Vec = Vec(-x, -y, -z) // Le blanc avant : est nécessaire (désolé pour les yeux)
    def dot(v: Vec): Double = x*v.x + y*v.y + z*v.z
    def cross(v: Vec): Vec = Vec(y*v.z - v.y * z, z*v.x - v.z * x, x*v.y - v.x * y)
    // On utilise des val pour éviter de recalculer plusieurs fois la même chose
    // On utilise lazy pour éviter des calculs potentiellement inutiles
    lazy val unitaire: Vec = this*(1.0/norme)
    lazy val norme: Double = Math.sqrt(normeCarre).toDouble
    lazy val normeCarre: Double = x * x + y * y + z * z
}