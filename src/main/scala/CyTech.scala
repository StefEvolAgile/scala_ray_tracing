
import ray_tracing._
// Logo de l'école
object SetupCyTech {
    // Lettre C
    val sphere1 = Sphere(Vec(-8, 20, 5), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere2 = Sphere(Vec(-9, 20, 5), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere3 = Sphere(Vec(-10, 20, 4), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere4 = Sphere(Vec(-10, 20, 3), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere5 = Sphere(Vec(-10, 20, 2), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere6 = Sphere(Vec(-8, 20, 1), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere7 = Sphere(Vec(-9, 20, 1), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    // Lettre Y
    val sphere8 = Sphere(Vec(-6, 20, 5), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere9 = Sphere(Vec(-5, 20, 4), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere10 = Sphere(Vec(-4, 20, 5), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere11 = Sphere(Vec(-5, 20, 3), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere12 = Sphere(Vec(-5, 20, 2), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere13 = Sphere(Vec(-5, 20, 1), 0.5, new Matiere(Couleur.blanc, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    // Lettre T
    val sphere14 = Sphere(Vec(0, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere18 = Sphere(Vec(1, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere16 = Sphere(Vec(2, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere17 = Sphere(Vec(1, 20, 4), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere15 = Sphere(Vec(1, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere19 = Sphere(Vec(1, 20, 2), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere20 = Sphere(Vec(1, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    // Lettre E
    val sphere21 = Sphere(Vec(4, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere22 = Sphere(Vec(5, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere23 = Sphere(Vec(6, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere24 = Sphere(Vec(4, 20, 4), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere25 = Sphere(Vec(4, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere26 = Sphere(Vec(5, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere27 = Sphere(Vec(4, 20, 2), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere28 = Sphere(Vec(4, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere29 = Sphere(Vec(5, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere30 = Sphere(Vec(6, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    // Lettre C
    val sphere31 = Sphere(Vec(9, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere32 = Sphere(Vec(10, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere33 = Sphere(Vec(8, 20, 4), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere34 = Sphere(Vec(8, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere35 = Sphere(Vec(8, 20, 2), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere36 = Sphere(Vec(9, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere37 = Sphere(Vec(10, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    // Lettre H
    val sphere38 = Sphere(Vec(12, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere39 = Sphere(Vec(12, 20, 4), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere40 = Sphere(Vec(12, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere41 = Sphere(Vec(12, 20, 2), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere42 = Sphere(Vec(12, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere43 = Sphere(Vec(13, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere44 = Sphere(Vec(14, 20, 1), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere45 = Sphere(Vec(14, 20, 2), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere46 = Sphere(Vec(14, 20, 3), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere47 = Sphere(Vec(14, 20, 4), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))
    val sphere48 = Sphere(Vec(14, 20, 5), 0.5, new Matiere(Couleur.bleuCY, Couleur.blanc, Couleur.blanc, Couleur.blanc, 32, 0.5))

    val listeCYTECH = List(sphere1, sphere2, sphere3, sphere4, sphere5, sphere6, sphere7, sphere8,
        sphere9, sphere10, sphere11, sphere12, sphere13, sphere14, sphere15, sphere16, sphere17, sphere18, sphere19,
        sphere20, sphere21, sphere22, sphere23, sphere24, sphere25, sphere26, sphere27, sphere28, sphere29, sphere30,
        sphere31, sphere32, sphere33, sphere34, sphere35, sphere36, sphere37, sphere38, sphere39, sphere40, sphere41,
        sphere42, sphere43, sphere44, sphere45, sphere46, sphere47, sphere48)
}
