package ray_tracing
import java.io._
import java.math._

// Classe représentant un pixel en fonction de ses valeurs RGB (Rouge Vert Bleu)
case class Pixel(r: Int, v: Int, b: Int) {
    // méthode renvoyant une couleur en fonction d'un pixel
    def toCouleur(): Couleur = Couleur(r/255.0, v/255.0, b/255.0) 
    // constructeur à partir d'une couleur
    def this(c: Couleur) = this((c.r*255).toInt, (c.v*255).toInt, (c.b*255).toInt)
}

// Classe modélisant un rayon en fonction de son origine et de sa direction
case class Rayon(origine: Vec, direction: Vec) {
    // méthode calculant la position d'un rayon en fonction du temps
    def pos(t: Double) : Vec = origine + direction * t
}

// Classe modélisant une image en tant qu'ensemble de pixels
class Image(largeur: Int, hauteur: Int, pixels: Array[Array[Pixel]]) { 
    def creerFichier(nom: String) {
        val fichier = new File(nom + ".ppm")
        val writer = new PrintWriter(fichier)
        writer.write("P3\n")
        writer.write(largeur + " " + hauteur + "\n")
        writer.write("255\n")
        pixels.flatten.foreach(pixel => writer.write(pixel.r + " " + pixel.v + " " + pixel.b + "\n"))
        writer.close()
    }
}

// Classe modélisant la matière d'un élément
// Les paramètres sont issus du document de référence : 
    // https://physique.cmaisonneuve.qc.ca/svezina/nyc/note_nyc/NYC_CHAP_6_IMPRIMABLE_4.pdf
class Matiere(val couleurBase: Couleur, val kReflexionAmbiant: Couleur, val kReflexionDiffu: Couleur,
    val kReflexionSpeculaire: Couleur, val rugosite: Double, val plasticite: Double) {
        val couleurPlastique = couleurBase * (1 - plasticite) + Couleur(plasticite, plasticite, plasticite)
    }

// Classe abstraite décrivant un objet dans la scène
abstract class Element() {
    // Calcule l'impact d'un rayon sur l'élément
    // renvoie None si pas d'impact
    // renvoie le couple (t, element) avec t le temps auquel se produit l'impact et l'élement lui-même
    def estImpacte(rayon: Rayon): Option[(Double, Element)]

    // Calcule la couleur d'un impact en utilisant potentiellement des rayons secondaires
    // C'est pour cela qu'on repasse la scène et le traceur en paramètre
    // rayon et t indiquent le point d'impact du rayon
    def couleurImpact(rayon: Rayon, t: Double, scene: Scene, traceur: Traceur): Couleur
}

// Classe décrivant un élément de type Sphère défini par son centre, son rayon r et sa matière m 
case class Sphere(centre: Vec, r: Double, m: Matiere) extends Element() {
    def estImpacte(rayon: Rayon): Option[(Double, Element)] = {
        val rs0 = rayon.origine - centre
        val A = 1
        val B = 2 * rs0.dot(rayon.direction)
        val C = rs0.normeCarre - r * r
        val D = B * B - 4 * A * C
        if (D > 0 && B < 0 && (B * B > D)) Some((-B - Math.sqrt(D)) /(2*A), this) else None
    }
    def couleurImpact(rayon: Rayon, t: Double, scene: Scene, traceur: Traceur): Couleur = {
        val vecNormal = (rayon.pos(t) - centre).unitaire
        traceur.couleurImpact(rayon, t, scene, traceur, vecNormal, m)
    }
}

// Classe décrivant un élément de type Damier défini par son origine, 2 vecteurs de quadrillage u1 et u2,
//      et deux matières m1 et m2
case class Damier(origine: Vec, u1: Vec, u2: Vec, m1: Matiere, m2: Matiere) extends Element() {
    val vecNormal = u1.cross(u2).unitaire 
    def estImpacte(rayon: Rayon): Option[(Double, Element)] = {
        val n = u1.cross(u2)
        val t = n.dot(origine-rayon.origine)/n.dot(rayon.direction)
        if (t > 0) Some(t, this) else None
    }
    def couleurImpact(rayon: Rayon, t: Double, scene: Scene, traceur: Traceur): Couleur = {
        val v = rayon.pos(t) - origine
        val a = (v.dot(u1) / u1.norme).toInt
        val b = (v.dot(u2) / u2.norme).toInt
        val m = if((a+b)%2 == 0) m1 else m2
        traceur.couleurImpact(rayon, t, scene, traceur, vecNormal, m)
    }
}

// Classe modélisant une lampe selon sa position et son intensité
case class Lampe(position: Vec, intensite: Couleur)

// Classe modélisant un scène comme une liste d'éléments, une liste de lampes et une intensité ambiante
case class Scene(elements: List[Element], lampes: List[Lampe], intensiteAmbiante: Couleur)

// Classe modélisant une caméra selon sa position, sa direction et sa verticale
// theta représente le champ de vision en degrés 
// proche : distance au viewport
case class Camera(position: Vec, direction: Vec, verticale: Vec, theta: Double, proche: Double) {
    def rot(alpha: Double): Camera = { 
        val alphaR = alpha/180*Math.PI  // alpha en degrés
        Camera(position, direction * Math.cos(alphaR) + direction.cross(verticale) * Math.sin(alphaR),
                verticale, theta, proche)
    }
}

// Classe qui contenant la logique de traçage
// A dériver avec les algorithmes concrets (Ray Casting, Ray Tracing...)
abstract class Traceur(){

    // Calcul de l'image résultante pour une résolution, une scène et une caméra donnée
    def calculeImage(largeur: Int, hauteur: Int, scene : Scene, camera: Camera): Image

    // Calcul du point d'impact du rayon sur la scène
    // renvoie None si pas d'impact
    // renvoie le couple (t, element) avec t le temps auquel se produit l'impact et l'élement touché
    // Utilise la méthode estImpacte() des éléments de la scène
    def calculeCible(rayon: Rayon, scene: Scene): Option[(Double, Element)]
    
    // Calcule la couleur d'un impact en utilisant potentiellement des rayons secondaires
    // C'est pour cela qu'on repasse la scène et le traceur en paramètre
    // rayon et t indiquent le point d'impact du rayon
    // vecNormal indique le vecteur normal et m la matière au point d'impact
    def couleurImpact(rayon: Rayon, t: Double, scene: Scene, traceur: Traceur,
                                    vecNormal: Vec, m: Matiere): Couleur
}

// Classe contenant les algorithmes de Ray Casting
class RayCasting() extends Traceur {
    def calculeCible(rayon: Rayon, scene: Scene): Option[(Double, Element)] = {
        val impacts = scene.elements.map[Option[(Double, Element)]](a => a.estImpacte(rayon)) 
        impacts.foldLeft[Option[(Double, Element)]](None)( 
        (_, _) match {
            case (None, b) => b
            case (a, None) => a
            case (Some((ta, a)), Some((tb, b))) => if (ta < tb) Some(ta, a) else Some(tb, b)
        })
    }

    def calculeImage(largeur: Int, hauteur: Int, scene : Scene, camera: Camera): Image = {
        val H = 2.0 * camera.proche * Math.tan(Math.PI*camera.theta/180/2)
        val L = H * largeur / hauteur
        val u1 = camera.direction.cross(camera.verticale).unitaire * (L / largeur)
        val u2 = camera.direction.cross(u1).unitaire * (H / hauteur)
        val rIni = u1 * (-largeur / 2) + u2 * (-hauteur / 2)
        def calculePixel(li: Int, col: Int, scene: Scene) = {
            val r0 = camera.position + camera.direction * camera.proche + rIni + u1 * col + u2 * li
            val rayon = Rayon(r0, (r0 - camera.position).unitaire)
            val cible = calculeCible(rayon, scene)
            cible match {
                case Some((t, element)) => new Pixel(element.couleurImpact(rayon, t, scene, this)) 
                case None =>  Pixel(0, 0, 0)
            }
        }
        new Image(largeur, hauteur, 
            Array.tabulate[Pixel](hauteur, largeur)((li, col) => calculePixel(li, col, scene)))
    }

    def couleurImpact(rayon: Rayon, t: Double, scene: Scene, traceur: Traceur,
                                    vecNormal: Vec, m: Matiere): Couleur = {
        val lumAmbiant = m.couleurBase.mul(m.kReflexionAmbiant).mul(scene.intensiteAmbiante)
        val pointImpact = rayon.pos(t*0.99999999) //0.99999999 pour éviter d'être gêné par notre propre objet
        scene.lampes.foldLeft[Couleur](lumAmbiant)(
            (c, lampe) => {
                val vecL = (lampe.position - pointImpact).unitaire
                val rayon2 = Rayon(pointImpact, vecL)
                val tLum = (lampe.position - pointImpact).norme
                traceur.calculeCible(rayon2, scene) match {
                    case Some((ta, a)) if ta < tLum => c
                    case _ => {
                        val nDotL = vecNormal.dot(vecL)
                        val vecE = (-rayon.direction).unitaire
                        val vecH = (vecE + vecL).unitaire
                        val nDotH = vecNormal.dot(vecH)
                        c + 
                        (if (nDotL < 0) Couleur.noir else (lampe.intensite.mul(m.kReflexionDiffu) * nDotL).mul(m.couleurBase)) +
                        (if (nDotH < 0) Couleur.noir else (lampe.intensite.mul(m.kReflexionSpeculaire) * Math.pow(nDotH, m.rugosite)).mul(m.couleurPlastique))
                    }
                }
            }
        )
    }
}
